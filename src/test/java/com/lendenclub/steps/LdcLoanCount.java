package com.lendenclub.steps;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LdcLoanCount {
	WebDriver driver;

	public LdcLoanCount(WebDriver driver)
	{
		this.driver=driver;
		//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);			
		try {
			WebDriverWait wait = new WebDriverWait(driver, 120);
			WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[2]/div/div/div[2]/div/div/div[1]/div/div[1]/div[1]/div")));
			String header = element1.getText().toString();
			System.out.println("@@@@ " + header);
		} catch(Exception ex) { }
			
		try {
		WebDriverWait wait = new WebDriverWait(driver, 1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
	} catch (Exception ex) { }
		
		try {
			// Date format for screenshot file name
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			Date date = new Date();
			String fileName = df.format(date);
			File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file, new File("./LdcLoanCount/"+fileName+".jpg"));
		} catch(Exception e) { }

	}
}
