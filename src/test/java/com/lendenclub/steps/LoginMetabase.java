package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class LoginMetabase {
	WebDriver driver;
	String Url="https://ldc.lendenclub.com/question/3015";
//	String Url = "https://ldc.lendenclub.com/question/3015#eyJuYW1lIjoiTERDIC0gTG9hbiBpbiBwcm9jZXNzaW5nIHN0YXRlIiwiZGVzY3JpcHRpb24iOm51bGwsImRhdGFzZXRfcXVlcnkiOnsicXVlcnkiOnsic291cmNlLXRhYmxlIjoxMTEsImZpbHRlciI6WyJhbmQiLFsiPSIsWyJmaWVsZC1pZCIsMTMxOF0sIkxJU1RFRCIsIlBST0NFU1NJTkciXSxbIiE9IixbImZpZWxkLWlkIiwyODYzXSwiQlBFIl0sWyI-IixbImZpZWxkLWlkIiwxMzM2XSwiMjAyMS0xMC0wMSJdLFsiPSIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgRGFpbHlpbnZlc3RtZW50IixbImZpZWxkLWlkIiwxNDY3XV0sdHJ1ZV1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXSxbInN1bSIsWyJmaWVsZC1pZCIsMTMzOV1dXSwiYnJlYWtvdXQiOltbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwxMzM2XSwiZGF5Il0sWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMTMxNV0sImRheSJdLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIERhaWx5aW52ZXN0bWVudCIsWyJmaWVsZC1pZCIsMTQ2NV1dLFsiZGF0ZXRpbWUtZmllbGQiLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIERhaWx5aW52ZXN0bWVudCIsWyJmaWVsZC1pZCIsMTQ3MF1dLCJkYXkiXV0sImpvaW5zIjpbeyJmaWVsZHMiOiJhbGwiLCJzb3VyY2UtdGFibGUiOjEwMywiY29uZGl0aW9uIjpbIj0iLFsiZmllbGQtaWQiLDEzMTJdLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIERhaWx5aW52ZXN0bWVudCIsWyJmaWVsZC1pZCIsMTQ2MV1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgRGFpbHlpbnZlc3RtZW50In1dLCJvcmRlci1ieSI6W1siZGVzYyIsWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMTMzNl0sImRheSJdXV19LCJ0eXBlIjoicXVlcnkiLCJkYXRhYmFzZSI6Mn0sImRpc3BsYXkiOiJ0YWJsZSIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnsidGFibGUucGl2b3RfY29sdW1uIjoic3RhdHVzIiwidGFibGUuY2VsbF9jb2x1bW4iOiJjb3VudCJ9LCJvcmlnaW5hbF9jYXJkX2lkIjozMDE1fQ==";
	
	public LoginMetabase(WebDriver driver) throws InterruptedException {
		this.driver = driver;
		
//		driver.get("https://ldc.lendenclub.com/question/3015");
		driver.get("https://ldc.lendenclub.com/auth/login/password");
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.findElement(By.name("username")).sendKeys("saurabh.s@lendenclub.com");
		driver.findElement(By.name("password")).sendKeys("saurabh123");
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Thread.sleep(2000);
		System.out.println("Welcome to login page");
		
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.get(Url);
		Thread.sleep(2000);

		driver.manage().window().fullscreen();

	}
}
